package controllers;


import controllers.Secure.Security;
import models.User;
import play.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 00:54
 */
public class GuardSecurity extends Security {

    static boolean authenticate(String login, String password) {

        User user = User.findByUsernameOrEmail(login.toLowerCase());
        if (user != null && user.passwordHash.equals(User.hashPassword(password))) {
            if (user.isConfirmed()) {
                flash.success("secure.login");
                return true;
            } else {
                flash.error("secure.user.not.confirmed");
                return false;
            }
        } else {
            flash.error("secure.bad.credentials");
            return false;
        }
    }

    public static User connectedUser() {
        if (!isConnected()) {
            return null;
        } else {
            return User.findByUsernameOrEmail(connected());
        }

    }

    public static boolean check(String profile) {
        Logger.info("checking Auth profile: " + profile);
        if (profile.equalsIgnoreCase("admin")) {
            return connectedUser().isAdmin();
        }
        return false;
    }

    static void onCheckFailed(String profile) {
        Logger.error("unauthorized.access");
        flash.error("unauthorized.access");
        redirect("/");
    }

    static void onAuthenticated() {
        Logger.info("User logged in: " + connected());
    }

    static void onDisconnect() {
        Logger.info("User logged out: " + connected());
    }


}
