package controllers;

import jobs.PasswordResetMailer;
import jobs.ResendConfirmation;
import jobs.SendConfirmation;
import models.Confirmation;
import models.PasswordReset;
import models.User;
import play.Logger;
import play.cache.Cache;
import play.data.validation.Valid;
import play.data.validation.Validation;
import play.mvc.Controller;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 00:33
 */
public class Guard extends Controller {

    /**
     * wyświetla formularz zakładania konta
     */
    public static void register() {
        render();
    }

    /**
     * Dodaje nowego użytkownika
     */
    public static void doRegister(@Valid User newUser) {
        checkAuthenticity();

        if (validation.hasErrors()) {
            params.flash();
            validation.keep();
            register();
        } else {
            if (newUser.validateAndSave()) {
                Confirmation confirmation = new Confirmation();
                confirmation.user = newUser;
                confirmation.save();
                new SendConfirmation(confirmation).now();
            }
            flash.success("registration.success");

        }

        Application.index();
    }

    public static void profile() {
        if (!GuardSecurity.isConnected()) {
            redirect("/");
        }

        User me = GuardSecurity.connectedUser();

        render(me);

    }

    public static void editProfile(String firstname, String surname, Boolean showSurname) {

        if (showSurname == null) {
            showSurname = false;
        }

        Logger.debug("Zmiana danych profilu: %s, %s", firstname, surname);

        User me = GuardSecurity.connectedUser();
        me.firstname = firstname;
        me.surname = surname;
        me.save();

        Cache.delete("USER_FULLNAME_" + me.id);
        Cache.delete("USER_DISPLAYNAME_" + me.id);
        flash.success("Zapisano pomyślnie");
        profile();
    }

    public static void confirm(String uid) {

        Confirmation conf = Confirmation.find("byUUID", uid).first();
        if (conf == null) {
            flash.error("confirmation.uuid.wrong");
            register();
        } else {
            if (conf.user.isConfirmed()) {
                flash.error("confirmation.already.confirmed");
            } else {
                conf.user.confirm();
                flash.success("confirmation.success");
            }

            redirect("/login");

        }

    }

    public static void passwordResetForm() {
        render();
    }

    public static void doPasswordResetForm(String email) {
        User user = User.find("byEmail", email).first();
        if (user != null) {
            Logger.info("Created Password Reset Token for: " + user.email);
            PasswordReset reset = new PasswordReset();
            reset.user = user;
            reset.save();

            new PasswordResetMailer(reset).now();
            flash.success("secure.reset.email.send");

        } else {
            flash.error("secure.no.such.user");
            passwordResetForm();
        }
        redirect("/");
    }

    public static void resendConfirmation() {
        render();
    }

    public static void doResendConfirmation(String email) {
        User user = User.find("byEmail", email).first();
        if (user != null) {
            if (user.isConfirmed()) {
                flash.error("secure.resend.user.confirmed");
            } else {
                Confirmation conf = Confirmation.find("byUser", user).first();
                if (conf == null) {
                    conf = new Confirmation();
                    conf.user = user;
                    conf.save();
                }
                new ResendConfirmation(conf).now();
                flash.success("secure.resend.confirmation");
            }
        } else {
            flash.error("secure.no.such.user");
            resendConfirmation();
        }
        redirect("/");
    }

    public static void newPassword(String uid) {
        render(uid);
    }

    public static void doNewPassword(String uuid, String password, String passwordConfirmation) {

        validation.required(password).message("errors.password.required");
        validation.required(passwordConfirmation).message("errors.password.confirmation.required");

        if (!password.equals(passwordConfirmation))
            Validation.addError(passwordConfirmation, "errors.passwords.not.match");

        if (validation.hasErrors()) {
            validation.keep();
            newPassword(uuid);
        } else {
            PasswordReset reset = PasswordReset.find("byUuid", uuid).first();

            if (reset != null) {
                reset.user.password = password;
                reset.user.save();
                reset.delete();
                flash.success("secure.reset.password.changed");
            } else {
                flash.error("secure.reset.no.token");
            }

            redirect("/");
        }

    }

}
