package controllers;

import play.Play;
import play.mvc.Controller;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 16:28
 */
public class Pages extends Controller {

    public static void show(String pageName) {

        String filePath = String.format("app/views/Pages/%s.html", pageName);

        notFoundIfNull(Play.getVirtualFile(filePath));

        renderTemplate(filePath);

    }
}
