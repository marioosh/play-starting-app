package controllers;

import jobs.SendContactMessageJob;
import models.ContactMessage;
import play.data.validation.Valid;
import play.mvc.Controller;
import ugot.recaptcha.Recaptcha;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 01.06.2012
 * Time: 21:34
 */
public class ContactMessages extends Controller {

    public static void newMessage() {
        if (GuardSecurity.isConnected()) {


            flash("message.name", GuardSecurity.connectedUser().displayName());
            flash("message.email", GuardSecurity.connectedUser().email);

        }
        render();
    }

    public static void sendMessage(@Valid ContactMessage message, @Recaptcha String captcha) {
        checkAuthenticity();

        if (validation.hasErrors()) {
            params.flash();
            validation.keep();
            newMessage();
        } else {

            new SendContactMessageJob(message).now();

            flash.success("contact.form.success");

        }

        Application.index();

    }

}
