package models;

import play.data.validation.Email;
import play.data.validation.MaxSize;
import play.data.validation.Required;
import util.BaseModel;

import javax.persistence.Entity;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 01.06.2012
 * Time: 21:30
 */
@Entity
public class ContactMessage extends BaseModel {

    @Required(message = "Imię i nazwisko jest wymagane")
    public String name;

    @Required(message = "Adres email jest wymagany")
    @Email
    public String email;

    @Required(message = "Tytuł wiadomości jest wymagana")
    @MaxSize(100)
    public String subject;

    @MaxSize(2000)
    public String message;
}
