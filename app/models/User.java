package models;


import play.Logger;
import play.cache.Cache;
import play.data.validation.*;
import play.libs.Crypto;
import util.BaseModel;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 00:37
 */
@Entity
@Table(name = "usermodel")
public class User extends BaseModel {
// ------------------------------ FIELDS ------------------------------

    @Required(message = "user.username.required")
    @Unique(message = "user.username.unique")
    @MinSize(value = 5, message = "user.username.length")
    public String username;

    @Required(message = "user.email.required")
    @Email(message = "user.email.format")
    @Unique(message = "user.email.unique")
    public String email;

    @Transient
    @Required(message = "user.password.required")
    @MinSize(value = 6, message = "user.password.length")
    public String password;

    public String passwordHash;

    @Transient
    @Required(message = "user.passwordConfirmation.required")
    @Equals(value = "password", message = "user.passwords.equals")
    public String passwordConfirmation;

    public Date confirmationDate;

    /**
     * TRUE if it is GLOBAL user, and can login by admin section
     * Role of the this user can be defined separatelly
     */
    public boolean admin = false;

    public String firstname = "";

    public String surname = "";

    public static User findByUsernameOrEmail(String login) {
        return User.find("username like ? or email like ?", login, login).first();
    }

    public static String hashPassword(String pass) {
        return Crypto.passwordHash(pass);
    }

    // -------------------------- STATIC METHODS --------------------------

    public static List<User> findAdmins() {
        return User.find("byAdmin", true).fetch();

    }


    public boolean confirm() {
        if (isConfirmed()) {
            return false;
        } else {
            this.confirmationDate = new Date();
            save();
            return true;
        }
    }

    @SuppressWarnings("JpaAttributeMemberSignatureInspection")
    public boolean isConfirmed() {
        return confirmationDate != null;
    }

    public String displayName() {
        final String DISPLAYNAME_KEY = "USER_DISPLAYNAME_" + this.id;

        String displayName = (String) Cache.get(DISPLAYNAME_KEY);

        if (displayName == null) {
            displayName = "";
        }
        if (displayName.isEmpty()) {
            if ((firstname != null) && firstname.length() > 0) {
                displayName += firstname;
            }

            if (surname.length() > 0) {
                displayName += (" " + surname);
            }

            if (displayName.equals("")) {
                displayName = username;
            }
        } else {
            Logger.debug("Display name read from cache: " + displayName);

        }
        Cache.set(DISPLAYNAME_KEY, displayName, "24h");
        return displayName;

    }

    public void setPassword(String pass) {
        this.password = pass;
        this.passwordHash = hashPassword(pass);
    }

    public void setUsername(String uName) {
        this.username = uName.trim().toLowerCase();
    }

    @SuppressWarnings("JpaAttributeMemberSignatureInspection")
    public boolean isAdmin() {
        return admin;
    }



}
