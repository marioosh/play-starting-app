package models;

import play.libs.Codec;
import util.BaseModel;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 12:32
 */
@Entity
public class PasswordReset extends BaseModel {

    public String uuid;

    @OneToOne
    public User user;

    public PasswordReset() {
        this.uuid = Codec.UUID().replaceAll("-", "");
    }
}
