package notifiers;

import models.Confirmation;
import models.ContactMessage;
import models.PasswordReset;
import models.User;
import play.Play;

/**
 * Created by IntelliJ IDEA.
 * User: marioosh
 * Date: 12.09.2011
 * Time: 23:34
 */
public class Mailer extends play.mvc.Mailer {
    private static final String SENDER = Play.configuration.getProperty("application.mailer.senderName", "Mailer <mailer@example.com>");

    public static void sendConfirmation(Confirmation confirmation) {
        setSubject("Instrukcja aktywacji konta");
        addRecipient(formatEmail(confirmation.user));

        setFrom(SENDER);
        send(confirmation);
    }

    public static void sendPasswordReset(PasswordReset passwordReset) {
        setSubject("Instrukcja zmiany hasła");
        addRecipient(formatEmail(passwordReset.user));

        setFrom(SENDER);
        send(passwordReset);
    }


    public static void sendContactMessage(ContactMessage contactMessage, User admin) {
        setSubject("Wiadomość ze strony wybieram.to");
        addRecipient(admin.email);
        setFrom(SENDER);

        setReplyTo(String.format("%s <%s>", contactMessage.name, contactMessage.email));

        send(contactMessage);
    }

    private static String formatEmail(User user) {
        return String.format("%s <%s>", user.displayName(), user.email);
    }

}
