package jobs;

import models.ContactMessage;
import models.User;
import notifiers.Mailer;
import play.jobs.Job;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 01.06.2012
 * Time: 22:17
 */
public class SendContactMessageJob extends Job {

    private ContactMessage contactMessage;


    public SendContactMessageJob(ContactMessage contactMessage) {
        this.contactMessage = contactMessage;
    }

    public void doJob() {
        for (User u : User.findAdmins()) {

            Mailer.sendContactMessage(contactMessage, u);

        }


    }
}
