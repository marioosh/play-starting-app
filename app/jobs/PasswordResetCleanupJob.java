package jobs;

import models.PasswordReset;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;

import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: marioosh
 * Date: 14.05.2012
 * Time: 10:41
 */
@Every("1h")
public class PasswordResetCleanupJob extends Job {

    public void doJob() {

        Logger.debug("Cleanup password reset tokens");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, -2);

        List<PasswordReset> resets = PasswordReset.find("createdAt < ?", cal.getTime()).fetch();
        for (PasswordReset reset : resets) {
            reset.delete();
        }

        Logger.info("Deleted %s old password reset tokens", resets.size());
    }
}
