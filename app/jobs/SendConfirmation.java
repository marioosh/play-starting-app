package jobs;

import models.Confirmation;
import notifiers.Mailer;
import play.Logger;
import play.jobs.Job;

/**
 * Created by IntelliJ IDEA.
 * User: mnosinski
 * Date: 02.02.12
 * Time: 09:49
 */
public class SendConfirmation extends Job {

    private Confirmation confirmation;

    public SendConfirmation(Confirmation conf) {

        this.confirmation = conf;
    }

    public void doJob() {

        Logger.info("Send confirmation to: " + confirmation.user.email);
        Mailer.sendConfirmation(confirmation);
    }

}
