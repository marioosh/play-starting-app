package jobs;

import models.Confirmation;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;

import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: marioosh
 * Date: 14.05.2012
 * Time: 21:45
 */
@Every("1d")
public class ConfirmationCleanupJob extends Job {

    public void doJob() {
        Logger.info("Clean up confirmations...");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        List<Confirmation> confirmations = Confirmation.find("createdAt < ?", cal.getTime()).fetch();

        for (Confirmation confirmation : confirmations) {
            confirmation.delete();
        }

        Logger.info("Deleted %s old confirmations", confirmations.size());
    }

}
