package jobs;

import models.PasswordReset;
import notifiers.Mailer;
import play.Logger;
import play.jobs.Job;

/**
 * Created by IntelliJ IDEA.
 * User: marioosh
 * Date: 03.02.2012
 * Time: 10:08
 */
public class PasswordResetMailer extends Job {

    private PasswordReset passwordReset;

    public PasswordResetMailer(PasswordReset pr) {

        passwordReset = pr;
    }

    public void doJob() {

        Logger.info("Sending password reset to %s", passwordReset.user.email);
        Mailer.sendPasswordReset(passwordReset);
    }

}
