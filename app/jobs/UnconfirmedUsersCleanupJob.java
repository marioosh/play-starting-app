package jobs;

import models.User;
import play.Logger;
import play.jobs.Every;
import play.jobs.Job;

import java.util.Calendar;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Mariusz Nosinski
 * Email: mnosinski@5dots.pl
 * Date: 14.05.2012
 * Time: 15:54
 */
@Every("1d")
public class UnconfirmedUsersCleanupJob extends Job {

    @Override
    public void doJob() {
        Logger.info("Clean up unconfirmed user accounts...");

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, -7);
        List<User> users = User.find("createdAd < ? AND confirmationDate IS NULL", cal.getTime()).fetch();

        for (User user : users) {
            user.delete();
        }

        Logger.info("Deleted %s unconfirmed user accounts", users.size());
    }
}
