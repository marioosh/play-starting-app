# --- !Ups
CREATE TABLE `usermodel`
(
id bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
createdAt datetime,
updatedAt datetime,
admin bit NOT NULL,
confirmationDate datetime,
email varchar(255),
firstname varchar(255),
passwordHash varchar(255),
registerType varchar(255),
showSurname bit NOT NULL,
surname varchar(255),
username varchar(255)
);

ALTER TABLE `usermodel` ADD INDEX `email` (`email`);
ALTER TABLE `usermodel` ADD INDEX `username` (`username`);

CREATE TABLE PasswordReset
(
  id bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
  createdAt datetime,
  updatedAt datetime,
  uuid varchar(255),
  user_id bigint,
  FOREIGN KEY (user_id) REFERENCES `usermodel` (id)
);

CREATE TABLE Confirmation
(
  id bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
  createdAt datetime,
  updatedAt datetime,
  uuid varchar(255),
  user_id bigint,
  FOREIGN KEY (user_id) REFERENCES `usermodel` (id)
);

ALTER TABLE Confirmation ADD INDEX `uuid` (`uuid`);

CREATE TABLE ContactMessage
(
  id bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
  createdAt datetime,
  updatedAt datetime,
  email varchar(255),
  message varchar(255),
  name varchar(255),
  subject varchar(255)
);


ALTER TABLE PasswordReset ADD INDEX `uuid` (`uuid`);

# --- !Downs

DROP TABLE Confirmation;
DROP TABLE ContactMessage;
DROP TABLE PasswordReset;
DROP TABLE `usermodel`;