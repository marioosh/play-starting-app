###Basement
based on playframework, current 1.2.5 version
included modules:
crud,
secure,
pagination,
recaptcha

###Sample Application

This few lines of code are extracted from one of my current projects. 

Every time I'm starting new project I have to do repeatable task such as:  

- Create User model,    
- allow user to register, login, logout
- add password reset,
- add confirm registration process by email,
- add resend confirmation option,
- create pages controller that displays pages in dynamic way,
- send contact messages to site admins,
- add twitter bootstrap css and javascripts,

###Instalation

Just clone this repo and it's ready to start

###What's done

BaseModel - model used as superclass, this model add two audits fields: createDate and modificationDate
User - basic User model.

Few helpers models

registration form - with basic credentials, after registration confirmation by email is needed,

login form - for logging in you can use username or email

password reset form - password reset process
resend confirmation form - to resend confirmation after register,

contact form - to send messages to admins,

Pages controller for dynamic pages.

###Configuration
Set everything in application.conf
change messages

you can add or change anything you want.

###Pages controller

If you want some semistatic pages, that aren't changed very often or almost never (for example FAQ, or privacy policy page)

How to use it:

Just put you page into `app/views/Pages` directory with .html extension.

your page is accessible by url/pages/file_name 

Example:

in pages you have file /app/views/Pages/faq.html
this file is rendered http://{..}/pages/faq


###TODO
A lot of text messages are hardcoded, additionally they're in Polish :) need to externalize into messages file and add english copy.



